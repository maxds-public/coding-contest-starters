import org.apache.camel.builder.RouteBuilder;

public class Routes extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("platform-http:/?httpMethodRestrict=GET")
                .log("incoming request: ${headers.CamelHttpUri}")
                .setBody(constant("Hello world"))
        ;
    }
}
