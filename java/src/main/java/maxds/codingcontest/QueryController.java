package maxds.codingcontest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class QueryController {

    @GetMapping
    String get(@RequestParam("q") String query) {
        System.out.println("Query is: " + query);
        return query;
    }
}
