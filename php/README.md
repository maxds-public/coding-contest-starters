PHP vanilla starter
===================

Start server: `php -S 0.0.0.0:8080`

Start server using docker `docker run --rm -p 8080:8080 -it php:8.1-cli php -S localhost:8080 index.php`