Python starter
==============

## Getting started

1. install python 3.10 and poetry
2. install the dependencies using `poetry install`
3. run the tests using `poetry run pytest`
4. launch the server using `poetry run start`

The app start listening on the default port: `8000`