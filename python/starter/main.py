import uvicorn
from fastapi import FastAPI
from fastapi.responses import PlainTextResponse

app = FastAPI()

@app.get("/", response_class=PlainTextResponse)
async def root(q: str = ""):
    """best endpoint ever"""
    return q if q else "Hola!" 

def start():
    """Launched with `poetry run start` at root level"""
    uvicorn.run("starter.main:app", host="0.0.0.0", port=8000, reload=True)